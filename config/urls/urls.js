/*jshint esversion: 6 */

/**
 * @author Gakuru Wanja,
 * 
 * @author Gakuru Maina,
 * 
 * @author Gakuru Njoki
 */

const urls = require('./baseUrls');
const baseUrl = urls.baseUrl;
const reportsUrl = urls.reportsUrl;
const listingsBaseUrl = urls.listingsBaseUrl;

module.exports = {
    baseUrl: baseUrl,
    listingsBaseUrl: listingsBaseUrl,
    reportsUrl: `${reportsUrl}`,

    fuelTypeUrl: `${baseUrl}/fueltypes`,
    storagesURL: `${baseUrl}/storages`,
    dropsURL: `${baseUrl}/drops`,
    shiftsURL: `${baseUrl}/shifts`,
    pumpsURL: `${baseUrl}/pumps`,
    readingsURL: `${baseUrl}/readings`,

    utilityUrl: `${baseUrl}/utility`,
    utilityReadingUrl: `${baseUrl}/utility/readings`,
    tenantUrl: `${baseUrl}/tenant`,
    releasedTenantUrl: `${baseUrl}/tenant/released`,
    tenantAccruedBalanceUrl: `${baseUrl}/tenant/arrears`,
    invoiceUrl: `${baseUrl}/invoice`,
    receiptUrl: `${baseUrl}/receipt`,
    arrearsUrl: `${baseUrl}/arrears`
}