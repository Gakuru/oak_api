/*jshint esversion: 6 */

"use strict";

const connection = require('./mysql');

const queryBuilder = {
  result: undefined,
  fields: undefined,
  tableName: undefined,
  query: undefined,
  params: undefined,

  // Construct
  init: (model, params) => {
    this.tableName = model;
    this.params = params;
    this.orderByParams = '';
    return queryBuilder;
  },
  select: (fields) => {
    this.fields = fields.split(',');
    queryBuilder.composeQuery();
    return queryBuilder;
  },
  where: (lh, predicate, rh) => {
    var part = " WHERE " + lh + " " + predicate + " " + rh;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  whereBetween: (field, a, b) => {
    var part = " WHERE " + field + " BETWEEN " + a + " AND " + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  andWhere: (lh, predicate, rh) => {
    var part = " AND " + lh + " " + predicate + " " + rh;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  andWhereBetween: (field, a, b) => {
    var part = " AND " + field + " BETWEEN " + a + " AND " + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  orWhere: (lh, predicate, rh) => {
    var part = " OR " + lh + " " + predicate + " " + rh;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  orWhereBetween: (field, a, b) => {
    var part = " OR " + field + " BETWEEN " + a + " AND " + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  // Joins
  join: (joinedTable, a, b) => {
    var part = " JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  innerJoin: (joinedTable, a, b) => {
    var part = " INNER JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  outerJoin: (joinedTable, a, b) => {
    var part = " OUTER JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  leftOuterJoin: (joinedTable, a, b) => {
    var part = " LEFT OUTER JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  rightOuterJoin: (joinedTable, a, b) => {
    var part = " RIGHT OUTER JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  leftJoin: (joinedTable, a, b) => {
    var part = " LEFT JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  rightJoin: (joinedTable, a, b) => {
    var part = " RIGHT JOIN " + joinedTable + " ON " + joinedTable + "." + a + " = " + this.tableName + "." + b;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  limit: (start, offset) => {
    var part = " LIMIT " + start + ", " + offset;
    queryBuilder.composeQuery(part);
    return queryBuilder;
  },
  orderByAsc: () => {

  },
  orderByDesc: () => {

  },
  // composes the query
  composeQuery: (part) => {
    var a = this.query;
    if (part) {
      if (this.query)
        this.query = a + part;
    } else {
      this.query = "SELECT " + this.fields + " FROM " + this.tableName;
    }
  },
  // get the query
  getQuery: () => {
    return this.query;
  },
  // execute the query
  execute: (callback) => {
    callback(connection.query(this.query, this.params, (err, result, fields) => {
      if (err) throw err;
      this.result = result;
    }))
  },
  // returns the data
  get: () => {
    queryBuilder.execute(() => {
      console.info(this.result);
      // return this.result;
    });
  }
};

module.exports = queryBuilder;