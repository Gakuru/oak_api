const pager = module.exports = {
    options: {
        offSet: 0,
        limit:10
    },
    /** @depricate - Will be depricated in favor of the v2 pager */
    getPager: (popped, nextStart, url) => {

        let nextStartAt = parseInt(nextStart) + 1;
        let currStartAt = (nextStartAt - 1);
        let hasNext = popped ? true : false;
        let hasPrev = (currStartAt - 1) <= 0 ? false : true;
        
        return {
            hasNext: hasNext,
            hasPrev: hasPrev,
            currStartAt: currStartAt,
            nextStartAt: nextStartAt,
            nextPageUrl: (hasNext
                ? url + '/page/' + nextStartAt
                : null),
            currentPageUrl: (!(hasNext && hasPrev))
                ? (nextStart > 1
                    ? `${url}/page/${ (nextStartAt - 1)}`
                    : url)
                : `${url}/page/${ (nextStartAt - 1)}`,
            prevPageUrl: (hasPrev
                ? url + '/page/' + (nextStartAt - 2)
                : null),
            pageSize: pager.options.limit
        }
    },
    getPagerV2: (popped, nextStart, url, searchTerm) => {
        let nextStartAt = parseInt(nextStart) + 1;
        let currStartAt = (nextStartAt - 1);
        let hasNext = popped ? true : false;
        let hasPrev = (currStartAt - 1) <= 0 ? false : true;
        
        return {
            hasNext: hasNext,
            hasPrev: hasPrev,
            currStartAt: currStartAt,
            nextStartAt: nextStartAt,
            nextPageUrl: (hasNext
                ? `${url}/${nextStartAt}${searchTerm?`/${searchTerm}`:''}`
                : null),
            currentPageUrl: (!(hasNext && hasPrev))
                ? (nextStart > 1
                    ? `${url}/${(nextStartAt-1)}${searchTerm?`/${searchTerm}`:''}`
                    : `${url}/${(nextStartAt-1)}${searchTerm?`/${searchTerm}`:''}`)
                : `${url}/${(nextStartAt-1)}${searchTerm?`/${searchTerm}`:''}`,
            prevPageUrl: (hasPrev
                ? `${url}/${(nextStartAt-2)}${searchTerm?`/${searchTerm}`:''}`
                : null),
            pageSize: pager.options.limit
        }
    }
};