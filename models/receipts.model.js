const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const moment = require('moment');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const documentRepository = require('../repositories/document.repository');

const appUser = require('../auth/user');

const queryBuilder = require('../config/db/queryBuilder');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart,searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm ? urls.receiptUrl : `${urls.receiptUrl}/search/${searchTerm}`),
        data: transformer.transformReceipt(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_receipts` (?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_receipt` (?);',
    [commands.FIND]: 'CALL `find_receipts` (?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO invoices SET ?;',
    [commands.UPDATE]: 'UPDATE `invoices` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {            
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const invoice = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, offset, (pager.options.limit+1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    iterable: true,
                    message: 'Get receipts success',
                    receipts: createPager(result, page)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get receipts failed', receipts: err});
            });

        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {
            
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [appUser.user.pid, param, offset, (pager.options.limit+1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        iterable: true,
                        message: 'Get receipts success',
                        receipts: createPager(result, page, param)
                    });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Get receipts failed', receipts: err});
                });

        });
    }
}
