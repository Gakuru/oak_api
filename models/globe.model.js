const DB = require('./db');

const _ = require('lodash');

const uuid = require('uuid/v1');

const moment = require('moment');

const commands = require('../config/commands/commands');

const transformer = require('../transformers/transformer');

const urls = require('../config/urls/urls');

const pager = require('../pager/pager');


const createCommRoutesPager = (result, nextStart, searchTerm, url) => {
    url = url.split('/').splice(1, 3).join('/');
    let popped = (result.length > pager.options.limit);
    if (popped) 
        result.pop();
    return {
        pager: pager.getPagerV2(popped, nextStart, `${urls.baseUrl}/${url}`, searchTerm),
        data: transformer.transformCommRoute(result)
    };
};

const createAdminUnitPager = (result, nextStart, searchTerm, url) => {
    url = url.split('/').splice(1, 3).join('/');
    let popped = (result.length > pager.options.limit);
    if (popped) 
        result.pop();
    return {
        pager: pager.getPagerV2(popped, nextStart, `${urls.baseUrl}/${url}`, searchTerm),
        data: transformer.transformAdministrativeUnit(result)
    };
};


const reduceRoutes = (data) => {
    let options = [];
    for (let key in data) {
        if (data[key])
            options.push(data[key]);
    }
    switch (options.length) {
        case 1 :
            {
                return {
                    discriminator: data.discriminator,
                    page: data.page ? parseInt(data.page):1
                };
            }
        case 2:
            {
                // Page the items
                if (isNaN(data.page))
                    return {
                        discriminator: data.discriminator,
                        page: 1,
                        searchTerm: data.page,
                        command: commands.FIND
                    };
                else
                    return {
                        discriminator: data.discriminator,
                        page: data.page ? parseInt(data.page):1
                    };
            }
        case 3:
            {
                // Use the searchterm
                return {
                    discriminator: data.discriminator,
                    page: parseInt(data.page),
                    searchTerm: data.searchTerm,
                    command: commands.FIND
                };
            }
        default:
            {
                // parameters unknown
                return false;
            }
    }
}

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_administrative_units` (?,?,?);',
    [commands.FIND]: 'CALL `find_administrative_units` (?,?,?,?);',
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const globe = module.exports = {
    getCommRoutes: ({url, data, command=commands.GET}) => {
        return new Promise((resolve, reject) => {
            const reduced = reduceRoutes(data);
            if (reduced) {
                command = reduced.command ? reduced.command : commands.GET;
                let offset = ((reduced.page * pager.options.limit) - pager.options.limit);
                let data = [];
                switch (command) {
                    case commands.FIND:
                        {
                            data = [reduced.discriminator, reduced.searchTerm, offset, (pager.options.limit + 1)];
                            break;
                        }
                    default:
                        {
                            data = [reduced.discriminator, offset, (pager.options.limit + 1)];
                            break;
                        }
                }

                const buildQuery = (command) => ({
                    [commands.GET]: 'CALL `get_globe_comm_routes` (?,?,?);',
                    [commands.FIND]: 'CALL `find_globe_comm_routes` (?,?,?,?);',
                })[command];

                let query = buildQuery(command);

                DB.exec(query, data).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: `Get ${reduced.discriminator} success`,
                        data: createCommRoutesPager(result, reduced.page, reduced.searchTerm, url)
                    });
                }).catch((err) => {
                    return reject({code: 404, success: false, message: `Get ${reduced.discriminator} failed`, data: err});
                });
            }
        });
    },
    getKeModel: ({url, data, command=commands.GET}) => {
        return new Promise((resolve, reject) => {
            const reduced = reduceRoutes(data);
            if (reduced) {
                command = reduced.command ? reduced.command : commands.GET;
                let offset = ((reduced.page * pager.options.limit) - pager.options.limit);
                let data = [];
                switch (command) {
                    case commands.FIND:
                        {
                            data = [reduced.discriminator, reduced.searchTerm, offset, (pager.options.limit + 1)];
                            break;
                        }
                    default:
                        {
                            data = [reduced.discriminator, offset, (pager.options.limit + 1)];
                            break;
                        }
                }
                let query = queryConfig(data, command);
                DB.exec(query.next().value, query.next().value).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: `Get ${reduced.discriminator} success`,
                        data: createAdminUnitPager(result, reduced.page, reduced.searchTerm, url)
                    });
                }).catch((err) => {
                    return reject({code: 404, success: false, message: `Get ${reduced.discriminator} failed`, data: err});
                });
            }
        });
    }
}
