const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const queryBuilder = require('../config/db/queryBuilder');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped) 
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.buildingUrl
            : `${urls.buildingUrl}/search/${searchTerm}`),
        data: transformer.transformBuilding(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_buildings` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_building` (?);',
    [commands.FIND]: 'CALL `find_buildings` (?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO buildings SET ?;',
    [commands.UPDATE]: 'UPDATE `buildings` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const sendEmailToOwner = (data) => {
    const nodemailer = require('nodemailer');
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.GMAIL_USERNAME,
            pass: process.env.GMAIL_PASSWORD
        }
    });
    
    const mailOptions = {
        from: 'RenterKe <renter.co.ke@gmail.com>',
        to: data.ownerEmail,
        subject: `Regarding your Property - ${data.name}`,
        html: `<div>
        <div>
            Hello ${data.ownerName},
        </div>
        <div>
            <p>
                Your property ${data.name} has been added to the list of properties managed by ${appUser.user.name}.
                They use <a href="https://renter.co.ke">Renterke</a> to simplyfy their operations
            </p>
        </div>
        <div>
            <p>Cheers,<br />Renterke Team</p>            
        </div>
        <div>
            Support
            <a href="mailto:info.renter.co.ke@gmail.com?Subject=Support" target="_top">Click here to send us an email</a>
            <br />
            Or
            <br />
            Write to us at info.renter.co.ke@gmail.com or renter.co.ke@gmail.com
        </div>
    </div>`
      };

      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}

const building = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, appUser.user.sid, offset, (pager.options.limit + 1)];
            
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get buildings success',
                    buildings: createPager(result, page)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get buildings failed', buildings: err});
            });

        });
    },
    find: ({
        param,
        page = 1,
        command = commands.FIND
    }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [
                appUser.user.pid,
                param,
                offset,
                (pager.options.limit + 1)
            ];
            let query = queryConfig(data, command);

            DB
                .exec(query.next().value, query.next().value)
                .then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get buildings success',
                        buildings: createPager(result, page, param)
                    });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Get buildings failed', buildings: err});
                });

        });
    },
    save: ({ data, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
            
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {

                    if (command === commands.UPDATE) 
                        Object.assign({}, ...data);

                    if(command == commands.SAVE && data.ownerEmail !== undefined)
                        // Send an email to the owner
                        sendEmailToOwner(data)
                    
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Save building success',
                        buildings: {
                            data: transformer.transformBuilding([data])
                        }
                    });
                })
                .catch((err) => {
                    throw err;
                    return reject({code: 404, success: false, message: 'Save building failed', buildings: err});
                });
        });
    },
    saveMetadata: ({ data }) => {
        return new Promise((resolve, reject) => {
            return resolve({
                code: 200,
                success: true,
                message: 'Save building success',
                buildings: {
                    data: [data]
                }
            });            
            // let query = queryConfig(data, command);
            // DB.raw(query.next().value, query.next().value).then((result) => {

            //         if (command === commands.UPDATE) 
            //             Object.assign({}, ...data);
                    
            //         return resolve({
            //             code: 200,
            //             success: true,
            //             message: 'Save building success',
            //             buildings: {
            //                 data: transformer.transformBuilding([data])
            //             }
            //         });
            //     })
            //     .catch((err) => {
            //         throw err;
            //         return reject({code: 404, success: false, message: 'Save building failed', buildings: err});
            //     });
        });
    },
    remove: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {
                building.get({}).then((buildings) => {
                        return resolve({code: 200, success: true, message: 'Remove buildings success', buildings: buildings.buildings});
                }).catch((err) => {
                        return reject(err);
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Remove buildings failed', buildings: err});
            });
        });
    }
}
