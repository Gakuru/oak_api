
const uuid = require('uuid/v4');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.fuelTypeUrl
            : `${urls.fuelTypeUrl}/search/${searchTerm}`),
        data: transformer.transformFuelType(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_fuelTypes` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_fuelTypes` (?);',
    [commands.FIND]: 'CALL `find_fuelTypes` (?,?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO `fuel_types` SET ?;;',
    [commands.UPDATE]: 'UPDATE `fuel_types` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.sid = appUser.user.sid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function* queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const fuelType = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid,appUser.user.sid, offset, (pager.options.limit + 1)];
            
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get fuel types success',
                    fuelTypes: createPager(result, page)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get fuel types failed', fuelTypes: err });
            });
        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [
                appUser.user.pid,
                param,
                offset,
                (pager.options.limit + 1)
            ];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get fuelTypes success',
                    fuelTypes: createPager(result, page, param)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get fuelTypes failed', fuelTypes: err });
            });

        });
    },
    save: ({ data, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {

                if (command === commands.UPDATE)
                    Object.assign({}, ...data);

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Save fuel type success',
                    fuelTypes: {
                        data: transformer.transformFuelType([data])
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Save fuel type failed', fuelTypes: err });
            });
        });
    },
    saveMetadata: ({ data }) => {
        return new Promise((resolve, reject) => {
            return resolve({
                code: 200,
                success: true,
                message: 'Save building success',
                fuelTypes: {
                    data: [data]
                }
            });
            // let query = queryConfig(data, command);
            // DB.raw(query.next().value, query.next().value).then((result) => {

            //         if (command === commands.UPDATE) 
            //             Object.assign({}, ...data);

            //         return resolve({
            //             code: 200,
            //             success: true,
            //             message: 'Save building success',
            //             fuelTypes: {
            //                 data: transformer.transformBuilding([data])
            //             }
            //         });
            //     })
            //     .catch((err) => {
            //         throw err;
            //         return reject({code: 404, success: false, message: 'Save building failed', fuelTypes: err});
            //     });
        });
    },
    remove: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {
                console.log(data);
                fuelType.get({}).then((fuelTypes) => {
                    return resolve({ code: 200, success: true, message: 'Remove fuel types success', fuelTypes: fuelTypes.fuelTypes });
                }).catch((err) => {
                    return reject(err);
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Remove fuel types failed', fuelTypes: err });
            });
        });
    }
}
