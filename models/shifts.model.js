
const uuid = require('uuid/v4');
const moment = require('moment');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.shiftsURL
            : `${urls.shiftsURL}/search/${searchTerm}`),
        data: transformer.transformShifts(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_shifts` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_shifts` (?);',
    [commands.FIND]: 'CALL `find_shifts` (?,?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO `shifts` SET ?;;',
    [commands.UPDATE]: 'UPDATE `shifts` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.sid = appUser.user.sid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function* queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const shifts = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, appUser.user.sid, offset, (pager.options.limit + 1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get shifts success',
                    shifts: createPager(result, page)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get shifts failed', shifts: err });
            });
        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [appUser.user.pid, param, offset, (pager.options.limit + 1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get shifts success',
                    shifts: createPager(result, page, param)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get shifts failed', shifts: err });
            });

        });
    },
    save: ({ data, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {

                if (command === commands.UPDATE)
                    Object.assign({}, ...data);

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Save shift success',
                    shifts: {
                        data: transformer.transformShifts([data])
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Save shift failed', shifts: err });
            });
        });
    },
    remove: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {
                shifts.get({}).then((shifts) => {
                    return resolve({ code: 200, success: true, message: 'Remove shifts success', shifts: shifts.shifts });
                }).catch((err) => {
                    return reject(err);
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Remove shifts failed', shifts: err });
            });
        });
    }
}
