const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const _ = require('lodash');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const queryBuilder = require('../config/db/queryBuilder');

const DB = require('./db');

const commands = require('../config/commands/commands');

const unit = require('./unit.model');

const utilityRepository = require('../repositories/utility.repository');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped) 
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.tenantUrl
            : `${urls.tenantUrl}/search/${searchTerm}`),
        data: transformer.transformTenant(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_tenants` (?,?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_tenant` (?);',
    [commands.FIND]: 'CALL `find_tenants` (?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO `tenants` SET ?;',
    [commands.UPDATE]: 'UPDATE `tenants` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            tenant.tenant.id = data.id;
            data.pid = appUser.user.pid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const saveTenantUtilities = (data) => {
    return new Promise((resolve, reject) => {
        const utilities = _.pick(data, 'utilities');

	if(utilities.utilities.length){
        utilities.utilities.forEach(utility => {
            const data = {
                id:uuid(),
                pid: appUser.user.pid,
                tenant_id: tenant.tenant.id,
                utility_id: utility
            };
            DB.raw('INSERT INTO `tenant_utilities` SET ?;', data).then((result) => {
                    return resolve(result);
            }).catch((err) => {
                return reject(err);
            });
        });
	}else{
	return resolve(true);
	}
    });
}

const tenant = module.exports = {
    tenant: {
        id: undefined,
        building_id: undefined,
        unit_id: undefined,
        first_name: undefined,
        last_name: undefined,
        id_no: undefined,
        mobile_no: undefined,
        email: undefined,
        current_tenant: undefined,
        utilities: undefined
    },
    get: ({ page = 1, id = undefined, released = false }) => {
        return new Promise((resolve, reject) => {

            let command = id ? commands.GET_BY_ID : commands.GET;
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id
                ? [id]
                : [
                    appUser.user.pid,
                    false,
                    released,
                    offset,
                    (pager.options.limit + 1)
                ];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value, connection).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get tenants success',
                    tenants: createPager(result, page)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get tenants failed', tenants: err});
            });

        });
    },
    getUtilities: ({ id }) => {
        return new Promise((resolve, reject) => {

            DB.exec('CALL `get_tenant_utilities` (?,?);', [id, appUser.user.pid]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get tenants utilities success',
                    tenantUtilities: {
                        data: transformer.transformTenantUtilities(result)
                    }
                });
            })
            .catch((err) => {
                return reject({code: 404, success: false, message: 'Get tenants utilities failed', tenants: err});
            });

        });
    },
    getLastUtilityReading: ({ data }) => {
        return new Promise((resolve, reject) => {

            DB.exec('CALL `get_last_utility_reading` (?,?,?);', [data.tenantId, data.utilityId, appUser.user.pid]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get tenants last reading success',
                    lastReading: {
                        data: transformer.transformLastTenantUtiltiyReading(result)
                    }
                });
            })
            .catch((err) => {
                return reject({code: 404, success: false, message: 'Get tenants last reading failed', lastReading: err});
            });

        });
    },
    navigateRecordSet: ({ data }) => {
        return new Promise((resolve, reject) => {
            DB.exec('CALL `navigate_tenants` (?,?,?)', [data.tenantId, appUser.user.pid, data.buildingId]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get tenants success',
                    tenants: {
                        data: result
                    }
                });
            }).catch((err) => {
              return reject({code: 404, success: false, message: 'Get tenants failed', tenants: err});
            });
        });
    },
    saveUtilityReading: ({ data }) => {
        return new Promise((resolve, reject) => {
            utilityRepository.recordUtilityReading({ data }).then((result) => {
                return resolve(result);
            }).catch((err) => {
                return reject(err);
            }); 
        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [
                appUser.user.pid,
                param,
                offset,
                (pager.options.limit + 1)
            ];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get tenants success',
                        tenants: createPager(result, page, param)
                    });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Get tenants failed', tenants: err});
                });

        });
    },
    save: ({ data, command = commands.SAVE }) => {

        return new Promise((resolve, reject) => {
            // extract the tenant specific attributes
            const _tenant = _.omit(data, 'utilities');

            let query = queryConfig(_tenant, command);
            DB.raw(query.next().value, query.next().value).then((result) => {

                    unit.save({
                        data: [
                            {
                                status: unit.status.occupied
                            }, {
                                id: data.unit_id
                            }
                        ],
                        command: commands.UPDATE,
                        transform: false
                    }).then((response) => {

                        saveTenantUtilities(data).then((result) => {

                            if (command = commands.SAVE) {
                                tenant.get({ id: tenant.id }).then((_tenant) => {

                                    const notificationCommands = require('../config/commands/notificationCommands');
                                    const notification = require('../models/notify.model');
                                    notification.checkSMSSetting(notificationCommands.TENANT_SAVED).then(toSMS => {
                                        _tenant = _.first(_tenant.tenants.data);
                                        if (_tenant.contactPhone) {                                            
                                            const mobileNo = _tenant.contactPhone.replace('0', '+254');                                            
                                            
                                            const message = `${_tenant.tenantName}, welcome to ${_tenant.buildingName} house ${_tenant.houseNo}`
            
                                            notification.sendSMS(_tenant.id, mobileNo, message);
                                        }
                                    });
                    
                                });
                            }

                            return resolve({
                                code: 200,
                                success: true,
                                message: 'Save tenant success',
                                tenants: {
                                    data: transformer.transformTenant([_tenant])
                                }
                            });

                        });

                    });
                })
                .catch((err) => {
                    throw err;
                    return reject({code: 404, success: false, message: 'Save tenant failed', tenants: err});
                });
        });
    },
    update: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {

            // extract the tenant specific attributes and format the data for update
            let _tenant = [_.omit(data, 'utilities', 'id'), { id: data.id }];
            tenant.tenant.id = data.id;
            
            let query = queryConfig(_tenant, command);

            DB.raw(query.next().value, query.next().value).then((result) => {
                DB.raw('DELETE FROM `tenant_utilities` WHERE `tenant_id` LIKE ?;', [tenant.tenant.id]).then((result) => {                    
                    saveTenantUtilities(data,command).then((result) => {
                        return resolve({
                            code: 200,
                            success: true,
                            message: 'Save tenant success',
                            tenants: {
                                data: transformer.transformTenant([_.omit(data, 'utilities')])
                            }
                        });
                    }).catch((err) => {
                        throw err;
                    });
                });
            }).catch((err) => {
                throw err;
                return reject({ code: 404, success: false, message: 'Save tenant failed', tenants: err });
            });
           
        });  
    },
    setTenantStatus: ({ data }) => {
        return new Promise((resolve, reject) => {
            tenant.get({id:data.id}).then((_tenant) => {
                _tenant = _.first(_tenant.tenants.data);
                DB.begin().then((connection) => {

                    DB.raw('update `tenants` set `current_tenant` = ?, `dismissed_at` = ? where `id` = ?;',
                        [!data.release, new Date(), _tenant.id],
                        connection
                    );

                    const unit = require('./unit.model');
                    DB.raw('update `units` set `status` = ? where id = ?;',
                        [unit.status.vacant, _tenant.uid],
                        connection);
                    
                    DB.commit(connection).then((result) => {
                        
                        tenant.get({}).then((tenants) => {
                            return resolve(tenants);
                        });

                    }).catch((err) => {
                        return reject({ code: 404, success: false, message: 'Save tenant failed', tenants: err });
                    });

                });
            });

        });
    }
}
