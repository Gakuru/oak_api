const connection = require('../config/db/mysql');

const appUser = require('../auth/user');

const DB = require('./db');

const Excel = require('exceljs');

const _ = require('lodash');


const analysis = module.exports = {

    getBuildingTemplate: () => {
        return new Promise((resolve, reject) => {

            connection.query('select name buildingName,location buildingLocation from buildings limit 1', (err, results, fields) => {
                if (err) throw err;

                const columns = fields.map((field) => {
                    return field.name
                });
                
                return resolve({
                    columns:columns
                });
            });            

        });
    },    
    getKeRuturnsExport: ({data}) => {
        return new Promise((resolve, reject) => {
            DB.exec('CALL kenya_rental_returns(?,?,?)', [appUser.user.pid, data.building_id, data.date]).then((results) => {
                
                let content = '';
                
                results.forEach((element,i) => {
                    content += `${element['Pin_of_Tenant']}\t${element['Name_of_Tenant']}\t${element['L_R_Number']}\t${element['Building']}\t${element['Street_or_Road']}\t${element['City_or_Town']}\t${element['County']}\t${element['District']}\t${element['Tax_Area_or_Locality']}\t${element['Nearest_Landmark']}\t${element['P_O_Box']}\t${element['Post_Office_Name']}\t${element['Postal_Code']}\t${element['Date_From']}\t${element['Date_To']}\t${element['Amount_of_Gross_Rental_Income']}\n`;
                });

                return resolve({
                    data:content
                });

                return resolve(true);

            }).catch((err) => {
                return reject(err);
            });
        });
    }
}
