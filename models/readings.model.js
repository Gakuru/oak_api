
const uuid = require('uuid/v4');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.readingsURL
            : `${urls.readingsURL}/search/${searchTerm}`),
        data: transformer.transformReadings(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_readings` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_readings` (?);',
    [commands.FIND]: 'CALL `find_readings` (?,?,?,?,?);',
    [commands.SAVE]: 'CALL `insert_reading` (?,?,?,?,?,?,?);',
    [commands.UPDATE]: 'UPDATE `readings` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.sid = appUser.user.sid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function* queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const readings = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, appUser.user.sid, offset, (pager.options.limit + 1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get readings success',
                    // readings: result
                    readings: createPager(result, page)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get readings failed', readings: err });
            });
        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [appUser.user.pid, param, offset, (pager.options.limit + 1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get readings success',
                    buildings: createPager(result, page, param)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get readings failed', buildings: err });
            });

        });
    },
    save: ({ data, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.sid = appUser.user.sid;
            DB.raw('call insert_reading(?,?,?,?,?,?,?,?)', [data.id, data.pid, data.sid, data.shiftId, data.noozleId, data.previousReading, data.currentReading, data.unitPrice]).then((result) => {
                if (command === commands.UPDATE)
                    Object.assign({}, ...data);

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Save reading success',
                    readings: {
                        data: transformer.transformPumps([data])
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Save reading failed', readings: err });
            });
        });
    },
    remove: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {
                readings.get({}).then((buildings) => {
                    return resolve({ code: 200, success: true, message: 'Remove buildings success', buildings: buildings.buildings });
                }).catch((err) => {
                    return reject(err);
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Remove buildings failed', buildings: err });
            });
        });
    }
}
