const uuid = require('uuid/v1');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.utilityUrl
            : `${urls.utilityUrl}/search/${searchTerm}`),
        data: transformer.transformUtilitiy(result)
    };
};

const createUtilityReadingsPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.utilityReadingUrl
            : `${urls.utilityReadingUrl}/search/${searchTerm}`),
        data: transformer.transformUtilitiyReadings(result)
    };
};

const createMonthlySummaryPager = (result, nextStart, searchTerm) => {
    if (result.length) {
        let popped = (result.length > pager.options.limit);
        if (popped)
            result.pop();
        return {
            pager: pager.getPager(popped, nextStart, !searchTerm
                ? urls.utilityReadingUrl
                : `${urls.utilityReadingUrl}/search/${searchTerm}`),
            data: transformer.transformMonthlyUtilitySummary(result)
        };
    } else {
        return result;
    }
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_utilities` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_utility` (?);',
    [commands.GET_BY_STATUS]: 'SELECT * FROM `utilities` WHERE `status` = ? AND `pid` = ?;',
    [commands.FIND]: 'CALL `find_utilities` (?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO utilities SET ?;',
    [commands.UPDATE]: 'UPDATE `utilities` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data
        },
        [commands.GET_BY_ID]: () => {
            return data
        },
        [commands.GET_BY_STATUS]: () => {
            return [data.status, appUser.user.pid];
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.mode = (data.mode
                ? utility.mode.billed
                : utility.mode.fixed);
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function* queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const utility = module.exports = {
    mode: {
        billed: 'billed',
        fixed: 'fixed'
    },
    get: ({
        page = 1,
        id = undefined,
        command = commands.GET
    }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id
                ? [id]
                : [
                    appUser.user.pid,
                    false,
                    offset,
                    (pager.options.limit + 1)
                ];
            let query = queryConfig(data, command);

            DB
                .exec(query.next().value, query.next().value)
                .then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get utilities success',
                        utilities: createPager(result, page)
                    });
                })
                .catch((err) => {
                    throw err;
                    return reject({ code: 404, success: false, message: 'Get utilities failed', utilities: err });
                });

        });
    },
    find: ({
        param,
        page = 1,
        command = commands.FIND
    }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [
                appUser.user.pid,
                param,
                offset,
                (pager.options.limit + 1)
            ];
            let query = queryConfig(data, command);

            DB
                .exec(query.next().value, query.next().value)
                .then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get utilities success',
                        utilities: createPager(result, page, param)
                    });
                })
                .catch((err) => {
                    return reject({ code: 404, success: false, message: 'Get utilities failed', utilities: err });
                });

        });
    },
    getUtilityReadings: ({ page = 1, data }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);

            DB.exec('CALL `get_utility_readings`(?,?,?,?)', [data, appUser.user.pid, offset, (pager.options.limit + 1)]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get utility readings success',
                    utilityReadings: ((condition) => {
                        switch (condition) {
                            case 'MONTHLY_SUMMARIES':
                                {
                                    return createMonthlySummaryPager(result, page);
                                }
                            default:
                                {
                                    return createUtilityReadingsPager(result, page);
                                }
                        }
                    })(data)
                });
            }).catch((err) => {
                throw err;
                return reject({ code: 404, success: false, message: 'Get utility readings failed', utilityReadings: err });
            });

        });
    },
    findUtilityReadings: ({ data, page = 1 }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            // let data = [appUser.user.pid, data, offset, (pager.options.limit+1)]; let
            // const query = queryConfig(data, command);

            DB.exec('CALL `find_utility_readings`(?,?,?,?)', [
                appUser.user.pid,
                data,
                offset,
                (pager.options.limit + 1)
            ]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get utility readings success',
                    utilityReadings: ((condition) => {
                        switch (condition) {
                            case 'monthly':
                                {
                                    return createMonthlySummaryPager(result, page, data);
                                }
                            default:
                                {
                                    return createUtilityReadingsPager(result, page, data);
                                }
                        }
                    })(data)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get utility readings failed', utilityReadings: err });
            });
        });
    },
    save: ({
        data,
        transform = true,
        command = commands.SAVE
    }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB
                .raw(query.next().value, query.next().value)
                .then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Save utility success',
                        utilities: {
                            data: transform
                                ? transformer.transformUtilitiy([data])
                                : [data]
                        }
                    });
                })
                .catch((err) => {
                    throw err;
                    return reject({ code: 404, success: false, message: 'Save utility failed', utilities: err });
                });
        });
    },
    remove: ({
        data,
        command = commands.UPDATE
    }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB
                .raw(query.next().value, query.next().value)
                .then((result) => {
                    utility
                        .get({})
                        .then((utilities) => {
                            return resolve({ code: 200, success: true, message: 'Remove utilities success', utilities: utilities.utilities });
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject({ code: 404, success: false, message: 'Remove utilities failed', utilities: err });
                });
        });
    }
}
