const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const moment = require('moment');

const _ = require('lodash');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const documentRepository = require('../repositories/document.repository');

const appUser = require('../auth/user');

const queryBuilder = require('../config/db/queryBuilder');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart,searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm ? urls.invoiceUrl : `${urls.invoiceUrl}/search/${searchTerm}`),
        data: transformer.transformInvoice(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_invoices` (?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_invoice` (?);',
    [commands.FIND]: 'CALL `find_invoices` (?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO invoices SET ?;',
    [commands.UPDATE]: 'UPDATE `invoices` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {            
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            return data;
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const invoice = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, offset, (pager.options.limit + 1)];
            command = id ? commands.GET_BY_ID : command;
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    iterable: true,
                    message: 'Get invoices success',
                    invoices: createPager(result, page)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get invoices failed', invoices: err});
            });

        });
    },
    getReceipts: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, offset, (pager.options.limit+1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    iterable: true,
                    message: 'Get invoices success',
                    invoices: createPager(result, page)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get invoices failed', invoices: err});
            });

        });
    },
    /***@todo Refactor to use vdocuments procedure */
    getInvoiceDetails: ({id = undefined, command = commands.GET_BY_ID }) => {
        return new Promise((resolve, reject) => {
            let data = [id]
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((_result) => {

                let result = transformer.transformInvoiceDetails(_result);
                let docHeader = _.first(result);

                DB.raw('SELECT `balance` FROM `tenant_balances` WHERE `tenant_id` LIKE ?', [docHeader.tenantId]).then((tenantCredit) => {
                    tenantCredit = _.first(tenantCredit);
                    let acHasCredit = false;
                    if (tenantCredit)
                        acHasCredit = tenantCredit.balance ? true : false;
    
                    let docSummary = {
                        docHeader: {
                            documentId: docHeader.documentId,
                            tenantId: docHeader.tenantId,
                            docDate: moment(docHeader.docDate).format('ddd, MMM Do, YYYY'),
                            docMonth: moment(docHeader.docDate).format('MMM, YYYY'),
                            docType: docHeader.docType,
                            docRef: docHeader.ref,
                            tenantName: docHeader.tenantName,
                            buildingName: docHeader.buildingName,
                            houseNo: docHeader.houseNo,
                            paidInFull: false,
                            acHasCredit: acHasCredit,
                            tenantCredit: acHasCredit ? tenantCredit.balance : 0
                        },
                        docTotals: {
                          totalBalance: 0,
                          paidToDate: 0,
                          grandTotal: 0
                        }
                      };
                
                    result.forEach(element => {
                        docSummary.docTotals.grandTotal += element.amount;
                        docSummary.docTotals.totalBalance += element.balance;
                        docSummary.docTotals.paidToDate += element.paidToDate;
                    });
    
                    if (docSummary.docTotals.paidToDate >= docSummary.docTotals.grandTotal)
                        docSummary.docHeader.paidInFull = true;
    
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get invoice details success',
                        invoiceDetails: {
                            pager: {},
                            data: {
                                docSummary: docSummary,
                                lineItems:result
                            }
                        }
                    });
                    
                });

            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get invoice details failed', invoices: err});
            });

        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {
            
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [appUser.user.pid, param, offset, (pager.options.limit+1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        iterable: true,
                        message: 'Get invoices success',
                        invoices: createPager(result, page, param)
                    });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Get invoices failed', invoices: err});
                });

        });
    },
    save: ({ data,command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
           
            switch (command) {
                case commands.SAVE: {
                    documentRepository.cleanupInvoices().then((result) => {
                        documentRepository.createInvoice({ data }).then((result) => {
                            // Email the newly created invoices
                            const amqp = require('amqplib/callback_api');
                            amqp.connect('amqp://localhost', (err, conn) => {
                                conn.createChannel((err, ch) => {
                                    if (err)
                                        throw err;

                                    const msgs = {
                                        EMAIL_NEW_INVOICES: 'EMAIL-NEW-INVOICES'
                                    }
                                    const dateParts = data.date.split(',');
                                    const docDate = moment().date(new Date().getDay).month(dateParts[0]).year(dateParts[1]).format('YYYY-MM-DD');
                                    const q = 'defaultQueue';
                                    const msg = JSON.stringify({msg:msgs.EMAIL_NEW_INVOICES,appUser:appUser.user,docDate:docDate});

                                    ch.assertQueue(q, { durable: false });
                                    ch.sendToQueue(q, Buffer.from(msg));
                                    setTimeout(() => { conn.close(); return; }, 500);
                                });
                            });
                            return resolve(result);
                        }).catch((err) => {
                            return reject(err);
                        }); 
                    }).catch((err) => {
                        return reject(err);
                    });
                    return;
                }
                case commands.PAY: {
                    documentRepository.payInvoice({ data }).then((result) => {
                        return resolve(result);
                    }).catch((err) => {
                        return reject(err)
                    });
                    return;
                }
            }
           
        });
    },
    remove: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB
                .raw(query.next().value, query.next().value)
                .then((result) => {
                    invoice
                        .get({})
                        .then((invoices) => {
                            return resolve({code: 200, success: true, iterable: true, message: 'Remove invoices success', invoices: invoices.invoices});
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Remove invoices failed', invoices: err});
                });
        });
    },
    notify: ({ data }) => {
        return new Promise((resolve, reject) => {
            invoice.getInvoiceDetails({ id: data.document_id }).then((invoice) => {
                notificationService({ notificationMethod: data.notificationMethod, data: invoice.invoiceDetails.data }).then((response) => {
                    return resolve(
                        {
                            code: 200,
                            success: true,
                            message: response.message,
                            notification:
                            {
                                sent:response.sent,
                                data:
                                {
                                    invoiceId: data.document_id,
                                    notificationMethod: data.notificationMethod
                                }
                            }  
                        }
                    );
                }).catch((err) => {
                    return reject(err);
                });
                
            }).catch((err) => {
                return reject(err);
            });
        });
    }
}

const notificationService = ({ notificationMethod, data }) => {
    return new Promise((resolve, reject) => {

        const { docHeader, docTotals } = data.docSummary;
        const tenant = require('./tenant.model');
        const _ = require('lodash');
        const moment = require('moment');
        const numeral = require('numeral');

        tenant.get({id:docHeader.tenantId}).then((result) => {

            const tenant = _.first(result.tenants.data);
            let response = {
                message: 'Invoice sent',
                sent:true,
            };

            switch (notificationMethod.toLowerCase().trim()) {
                case 'sms':
                    {
                        const mobileNo = tenant.contactPhone.replace('0', '+254');
                        const message = `${tenant.buildingName}, Hse: ${tenant.houseNo}. ${docHeader.docMonth} invoice Ref: ${docHeader.docRef} balance as at ${moment().format('DD-MM-YYYY')} is ${numeral(docTotals.totalBalance).format('0,0')}`;

                        const notification = require('./notify.model');

                        const sendSMSResponse = async () => {
                            try {
                                return await notification.sendSMS(tenant.id, mobileNo, message);
                            } catch (err) {
                                return await 'Failed to send SMS'
                            }
                        }
                        
                        sendSMSResponse().then((message) => {
                            if(_.has(message,'message'))
                                response.message = message.message;
                            response.sent = message.sent;
                            return resolve(response);
                        });
                        
                        break;
                        
                    }
                case 'email':
                    {
                        const qrCode = require('../utils/qrcodes').generateQrCode(docHeader.docRef);
                        const ip = require('ip');
                        const { lineItems } = data;

                        let emailData = {
                            email: tenant.contactEmail,
                            subject: `${tenant.buildingName}, House ${tenant.houseNo} - ${docHeader.docMonth} ${_.startCase(docHeader.docType)}, Ref: ${docHeader.docRef}`,
                            body: undefined,
                            attachments: []
                        }

                        const lines = lineItems.map((lineItem, i) => {
                            return (
                                `<tr style="border-bottom: #a5a5a5 1px solid; background-color:${i%2===0?'#fffff;':'#e5e5e5;'}">
                                    <td style="width:125px;">${lineItem.chargeOn}</td>                                    
                                    <td style="width:50px; text-align:center;">${lineItem.unitOfMeasure}</td>
                                    <td style="width:100px; text-align:right;">${lineItem.quantity}</td>
                                    <td style="width:110px; text-align:right;">${numeral(lineItem.unitCost).format('0,0')}</td>
                                    <td style="width:110px; text-align:right;">${numeral(lineItem.amount).format('0,0')}</td>
                                </tr>`
                            );
                        }).join('');

                        emailData.body = (`<div style="border:#e5e5e5 2px solid;padding:5px;height:610px; width:560px;">
                        <div style="text-align:center">
                          <strong>INVOICE</strong>
                        </div>
                        <div>
                          <hr style="border:#e5e5e5 1px solid;" />
                        </div>
                        <div style="height:150px;width:inherit; clear:both;">
                          <div style="width:100px; float:left;">
                            <img style="width:100px;" src="http://${ip.address()}:81/qrcodes/${qrCode}" alt="${docHeader.docRef}" />
                          </div>
                          <div style="width:200px; float:left; text-align:center;">
                            ${
                                docTotals.totalBalance ?
                                ``
                                :
                                `<div style="color:#008202">
                                    <h2>Paid In Full</h2>
                                </div>`
                            }
                          </div>
                          <div style="float:right; width:230px;">
                            <div style="clear:both;">
                              <div style="float:left;">
                                <div style="float:left;">
                                  <div style="padding:5px;">
                                    From
                                  </div>
                                </div>
                              </div>
                              <div style="float:left; border-left:#e5e5e5 2px solid;">
                                <div style="padding:5px;">
                                  <div>
                                    <strong>${appUser.user.name}</strong>
                                  </div>
                                  <div>
                                    ${tenant.buildingName}
                                  </div>
                                  <div>
                                    ${tenant.buildingLocation}
                                  </div>                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div style="height:100px;width:inherit; clear:both;">
                          <div style="width:300px; float:left;">
                            <div style="clear:left;">
                              <div style="float:left; border-right:#e5e5e5 2px solid;">
                                <div style="padding:5px;">
                                  <div>
                                    Invoice Ref
                                  </div>
                                  <div>
                                    Issue Date
                                  </div>
                                  <div>
                                    Subject
                                  </div>
                                </div>
                              </div>
                              <div style="float:left;">
                                <div style="padding:5px;">
                                  <div>
                                    ${docHeader.docRef}
                                  </div>
                                  <div>
                                    ${docHeader.docDate}
                                  </div>
                                  <div>
                                    ${docHeader.docMonth} <span style="text-transform: capitalize;">${docHeader.docType}</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div style="float:right; width:220px;">
                            <div style="clear:both;">
                              <div style="float:left;">
                                <div style="float:left;">
                                  <div style="padding:5px; width:30px">
                                    For
                                  </div>
                                </div>
                              </div>
                              <div style="float:left; border-left:#e5e5e5 2px solid;">
                                <div style="padding:10px;">
                                  <div>
                                    <strong>${tenant.tenantName}</strong>
                                  </div>
                                  <div>
                                    ${tenant.buildingName}
                                  </div>
                                  <div>
                                    ${tenant.houseNo}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div style="height:300px;width:inherit; clear:both;">
                          <div style="padding:5px;">                            
                            <table id="line-items" style="width:100%; border-collapse: collapse; vertical-align:middle;">
                              <thead>
                                <tr style="border-bottom: #a5a5a5 1px solid; background-color:#d5f5f5; text-align:left;">
                                  <th style="width:130px;">Item</th>
                                  <th style="width:50px; text-align:center;">UOM</th>
                                  <th style="width:100px; text-align:right;">Quantity</th>
                                  <th style="width:110px; text-align:right;">Unit Price</th>
                                  <th style="width:110px; text-align:right;">Line Total</th>
                                </tr>
                              </thead>
                              <tbody>
                                ${lines}
                                <!-- after the loop -->
                                <tr>
                                  <td colspan="5" style="text-align:right; border:none;">
                                    <div style="float:right; width:220px;">
                                      <table style="width:100%; border-collapse: collapse; vertical-align:middle;">
                                        <tbody>
                                          <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                                            <td style="width:100px;">Sub Total</span></td>
                                            <td>${numeral(docTotals.grandTotal).format('0,0')}</td>
                                          </tr>
                                          <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                                            <td style="width:100px;">Amount Paid</span></td>
                                            <td>${numeral(docTotals.paidToDate).format('0,0')}</td>
                                          </tr>
                                          <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                                            <td style="width:100px;">Amount Due</span></td>
                                            <td>${numeral(docTotals.totalBalance).format('0,0')}</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div style="height:50px;width:inherit; clear:both;">
                        <div>
                          <i>Powered by <a style="text-decoration:none;" href="https://renter.co.ke">RenterKe</a></i>
                        </div>
                        </div>
                      </div>
                      `);

                        const pdf = require('html-pdf');                      
                        const options = { format: 'A4' };

                        pdf.create(emailData.body, options).toStream((err, stream) => {
                            const fs = require('fs');
                            
                            if (err) return console.log(err);
    
                            stream.pipe(fs.createWriteStream(`/var/www/html/pdfdocs/${docHeader.docRef}.pdf`));
    
                            emailData.attachments.push({
                                filename: emailData.subject,
                                path: `/var/www/html/pdfdocs/${docHeader.docRef}.pdf`,
                                contentType: 'application/pdf'
                            });

                            const notification = require('./notify.model');

                            notification.sendEmail(emailData.email, emailData.subject, emailData.body,emailData.attachments).then((resp) => {
                                fs.unlinkSync(`/var/www/html/pdfdocs/${docHeader.docRef}.pdf`);
                                return resolve(response);
                            });
                        });
                        return resolve(response);
                    }
                default:
                    {
                        return reject('unknown notification service');
                    }
            }

        }).catch((err) => {
            return reject(err);
        });
    });
}