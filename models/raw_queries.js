const DB = require('./db');

const appUser = require('../auth/user');

module.exports = {
  analysePayment: ({  }) => {
    return new Promise((resolve, reject) => {
      const query = "CALL `analyse_payments`(?);";
      DB.exec(query, [appUser.user.pid]).then((result) => {
        return resolve(result);
      }).catch((err) => {
        return reject(err);
      });
    });
  },
  collectionsByPaymentMode: ({ year }) => {
    return new Promise((resolve, reject) => {
      const query = "CALL `analyse_collections_by_payment_mode`(?,?);";
      DB.exec(query, [appUser.user.pid,year]).then((result) => {
        return resolve(result);
      }).catch((err) => {
        return reject(err);
      });
    });
  },
  collectionsByUnitType: ({ year }) => {
    return new Promise((resolve, reject) => {
      const query = "CALL `analyse_collections_by_unit_type`(?,?);";
      DB.exec(query, [appUser.user.pid,year]).then((result) => {
        return resolve(result);
      }).catch((err) => {
        return reject(err);
      });
    });
  },
  collectionsByApartmentPerYear: ({ year }) => {
    return new Promise((resolve, reject) => {
      const query = "CALL `collection_analysis_per_building_per_year`(?,?);"
      DB.exec(query, [appUser.user.pid, year]).then((result) => {
        return resolve(result);
      }).catch((err) => {
        return reject(err);
      });
    });
  }
}