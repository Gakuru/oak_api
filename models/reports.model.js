const appUser = require('../auth/user');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');

const transformer = require('../transformers/transformer');

const createPager = (result, nextStart, searchTerm) => {
  return {
      pager: {},
      data: transformer.transformReports(result)
  };
};

module.exports = {
  getReports: () => {
    return new Promise((resolve, reject) => {
      const axios = require('axios');
      axios.get(`https://reports.renter.co.ke/jasperserver/rest_v2/resources`, {
        params: {
          j_username: 'renterke',
          j_password: 'sidhaniInaPassword!',
          type: 'reportUnit',
          folderUri: '/reports/RenterKe'
        }
      })
        .then((response) => {
          const reports = response.data.resourceLookup.map(report => {
            let reportUri = report.uri.split('/').join('-');
            return {
              reportName: report.label,
              reportDescription: report.description,
              previewUrl: `${urls.reportsUrl}/report/render/${reportUri}`
            }
          });
          /***@todo will fix */
          return resolve({
            code: 200,
            success: true,
            message: 'Get reports success',
            reports: createPager(reports)
          });
      })
      .catch(function (error) {
        return reject({
          code: 400,
          success: false,
          message: 'Get reports success',
          reports: error
        });
      });
    });
  },
  getReportUrl: ({ data }) => {
    return new Promise((resolve, reject) => {
      const uriParts = data.reporturi.split('-');
      const uriPartsLength = uriParts.length - 1;
      const reportParentUri = `${uriParts.splice(0, uriPartsLength).join('/')}`;
      const reportUnit = uriParts[0];
      const reportUrl = `https://reports.renter.co.ke/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=${reportParentUri}&reportUnit=${reportParentUri}/${reportUnit}&j_username=renterke&j_password=sidhaniInaPassword!&p_pid=${appUser.user.pid}`
      return resolve({reportUrl});
    });
  }
};
