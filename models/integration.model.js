const DB = require('./db');

const _ = require('lodash');

const uuid = require('uuid/v1');

const moment = require('moment');

const commands = require('../config/commands/commands');

const integration = module.exports = {
    processPaybill: ({data}) => {
        return new Promise((resolve, reject) => {

            // Format paybox date
            data.transactionDate = data.transactionDate.replace(/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/, '$1-$2-$3 $4:$5:$6');
            
            // Check if the payment is for topping up the account
            DB.raw('SELECT `id`,`user` FROM `parent` WHERE `user` LIKE ?', [_.last(data.invoiceRef.split('-'))]).then((user) => {
                if (!_.isEmpty(user)) {
                    user = _.first(user);
                    user.transDate = moment(data.transactionDate).format('YYYY-MM-DD');
                    DB.exec('CALL `insert_parent_journal`(?,?,?,?,?,?,?,?);', [uuid(), user.id, 'credit', 'receivable', data.amount,data.transactionRef,data.paymentMode,user.transDate]).then((result) => {
                        DB.exec('CALL `insert_parent_balance`(?,?,?);', [uuid(), user.id, data.amount]).then((result) => {
                            return resolve(user);
                        });
                    });
                } else {
                    DB.raw('SELECT `id`,`pid`,`tenant_id` FROM `documents` WHERE `doc_ref` LIKE ?', [data.invoiceRef]).then((doc) => {
                        if (!_.isEmpty(doc)) {
                            doc = _.first(doc);
            
                            const invoicePayment = {
                                document_id: doc.id,
                                pid:doc.pid,
                                tenant_id: doc.tenant_id,
                                amount: data.amount,
                                transactionRef: data.transactionRef,
                                receivingDate: moment(data.transactionDate).format('DD-MM-YYYY'),
                                receivingAcc: data.paymentMode
                            }
            
                            const invoice = require('./invoice.model');
            
                            invoice.save({data:invoicePayment, command:commands.PAY}).then((result) => {
                                return resolve({status:200,success:true,message:'Payment recorded'});
                            }).catch((err) => {
                                throw err;
                                return reject(err);
                            });                            
                        }
                });    

                }
                
            });

        });
    }
}
