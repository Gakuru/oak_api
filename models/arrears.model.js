const uuid = require('uuid/v1');

const moment = require('moment');

const _ = require('lodash');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const documentRepository = require('../repositories/document.repository');

const appUser = require('../auth/user');

const queryBuilder = require('../config/db/queryBuilder');

const DB = require('./db');

const numeral = require('numeral');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart,searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm ? urls.arrearsUrl : `${urls.arrearsUrl}/search/${searchTerm}`),
        data: transformer.transformTenantsWithArrears(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_tenant_arrears` (?,?,?);',
    // [commands.GET_BY_ID]: 'CALL `get_receipt` (?);',
    [commands.FIND]: 'CALL `find_tenant_arrears` (?,?,?,?);',
    // [commands.SAVE]: 'INSERT INTO invoices SET ?;',
    // [commands.UPDATE]: 'UPDATE `invoices` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {            
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const invoice = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid,offset, (pager.options.limit+1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    iterable: true,
                    message: 'Get tenants arrears success',
                    arrears: createPager(result, page)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get tenants arrears failed', arrears: err});
            });

        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {
            
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [appUser.user.pid, param, offset, (pager.options.limit+1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        iterable: true,
                        message: 'Get tenants arrears success',
                        arrears: createPager(result, page, param)
                    });
                })
                .catch((err) => {
                    throw err;
                    return reject({code: 404, success: false, message: 'Get tenants arrears failed', arrears: err});
                });

        });
    },
    getArrearsDetails: ({ param }) => {
        return new Promise((resolve, reject) => {

            DB.raw('select forMonth,charge_on,doc_ref,document_id,amount,linePaidToDate,lineItemBalance,tenantName,buildingName,unitNo from vdocumentitems2 WHERE lineItemBalance > ? AND tenant_id like ? order by doc_date desc', [0, param]).then((result) => {
                if (!_.isEmpty(result)) {
                    const arrearsDetails = _(result).groupBy('forMonth').map((monthlyGroup,month) => {
                        return {
                            month: month,
                            invoiceRef: _.first(monthlyGroup).doc_ref,                        
                            documentId: _.first(monthlyGroup).document_id,                        
                            grossAmount: numeral(_(monthlyGroup).sumBy('amount')).format('0,0.00'),
                            paidToDate: numeral(_(monthlyGroup).sumBy('linePaidToDate')).format('0,0.00'),
                            amountDue: numeral(_(monthlyGroup).sumBy('lineItemBalance')).format('0,0.00'),
                            lineItems: monthlyGroup.map(group => {
                                return {
                                    item: group.charge_on,
                                    amount: numeral(group.amount).format('0,0.00'),
                                    linePaidToDate: numeral(group.linePaidToDate).format('0,0.00'),
                                    balance: numeral(group.lineItemBalance).format('0,0.00')
                                }
                            })
                        }
                    });
    
                    const arrearsSummary = {
                        buildingName: _.first(result).buildingName,
                        houseNo: _.first(result).unitNo,
                        tenantName: _.first(result).tenantName,
                        totoalGross: _(result).sumBy('amount'),
                        totalPaidToDate: numeral(_(result).sumBy('linePaidToDate')).format('0,0.00'),
                        totalAmountDue: numeral(_(result).sumBy('lineItemBalance')).format('0,0.00')
                    }
    
                    return resolve({
                        code: 200,
                        success: true,
                        iterable: true,
                        message: 'Get tenants arrears details success',
                        arrearsDetails: {
                            arrearsSummary,
                            arrearsDetails
                        }
                    });
                }
            }).catch((err) => {
                throw err;
                return reject({code: 404, success: false, message: 'Get tenants arrears details failed', arrearsDetails: err});
            });

        });
    },
    getAllInArrears: ({ id = '%' }) => {
        return new Promise((resolve, reject) => {
            DB.raw('select forMonth,charge_on,doc_ref,document_id,amount,linePaidToDate,lineItemBalance,tenantEmail,tenantContactPhone,tenant_id,tenantName,buildingName,buildingLocation,unitNo from vdocumentitems2 WHERE lineItemBalance > ? AND pid = ? AND current_tenant = ? AND (tenant_id LIKE ? OR document_id LIKE ?) order by doc_date desc;', [0, appUser.user.pid, 1, id, id]).then((result) => {
                if (!_.isEmpty(result)) {
                    const arrearsDetails = _(result).groupBy('tenantName').map((tenantGroup,tenantName) => {
                        return {
                            tenantName: tenantName,
                            tenantId: _.first(tenantGroup).tenant_id,
                            buildingName: _.first(tenantGroup).buildingName,
                            buildingLocation: _.first(tenantGroup).buildingLocation,
                            houseNo: _.first(tenantGroup).unitNo,
                            tenantEmail: _.first(tenantGroup).tenantEmail,
                            tenantContactPhone: _.first(tenantGroup).tenantContactPhone,
                            fromMonth: _.last(tenantGroup).forMonth,
                            toMonth: _.first(tenantGroup).forMonth,
                            totalOpenInvoices: _.size(_(tenantGroup).groupBy('forMonth').value()),
                            totalGross: numeral(_(tenantGroup).sumBy('amount')).format('0,0.00'),
                            totalPaidToDate: numeral(_(tenantGroup).sumBy('linePaidToDate')).format('0,0.00'),
                            totalAmountDue: numeral(_(tenantGroup).sumBy('lineItemBalance')).format('0,0.00'),
                            lineItems: tenantGroup.map(group => {
                                return {
                                    month: group.forMonth,
                                    item: group.charge_on,
                                    amount: numeral(group.amount).format('0,0.00'),
                                    linePaidToDate: numeral(group.linePaidToDate).format('0,0.00'),
                                    balance: numeral(group.lineItemBalance).format('0,0.00')
                                }
                            })
                        }
                    }).value();
    
                    // const arrearsSummary = {
                    //     buildingName: _.first(result).buildingName,
                    //     houseNo: _.first(result).unitNo,
                    //     tenantName: _.first(result).tenantName,
                    //     totoalGross: _(result).sumBy('amount'),
                    //     totalPaidToDate: numeral(_(result).sumBy('linePaidToDate')).format('0,0.00'),
                    //     totalAmountDue: numeral(_(result).sumBy('lineItemBalance')).format('0,0.00')
                    // }
    
                    return resolve({
                        code: 200,
                        success: true,
                        iterable: true,
                        message: 'Get all in arrears success',
                        arrearsDetails: {
                            arrearsDetails
                        }
                    });
                }
            }).catch((err) => {
                throw err;
                return reject({code: 404, success: false, message: 'Get tenants arrears details failed', arrearsDetails: err});
            });

        });
    }
}
