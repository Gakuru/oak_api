const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped) 
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.unitUrl
            : `${urls.unitUrl}/search/${searchTerm}`),
        data: transformer.transformUnit(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_units` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_unit` (?);',
    [commands.GET_BY_STATUS]: 'SELECT * FROM `vunits` WHERE `status` = ? AND `pid` = ?;',
    [commands.FIND]: 'CALL `find_units` (?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO units SET ?;',
    [commands.UPDATE]: 'UPDATE `units` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data
        },
        [commands.GET_BY_ID]: () => {
            return data
        },
        [commands.GET_BY_STATUS]: () => {
            return [data.status, appUser.user.pid];
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.status = data.status ? unit.status.vacant : unit.status.occupied;
            return data;
        },
        [commands.UPDATE]: () => {            
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const unit = module.exports = {
    status: {
        occupied: 'occupied',
        vacant: 'vacant'
    },
    get: ({
        page = 1,
        id = undefined,
        command = commands.GET
    }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id
                ? [id]
                : [
                    appUser.user.pid,
                    false,
                    offset,
                    (pager.options.limit + 1)
                ];
            let query = queryConfig(data, command);

            DB
                .exec(query.next().value, query.next().value)
                .then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get units success',
                        units: createPager(result, page)
                    });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Get units failed', units: err});
                });

        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [
                appUser.user.pid,
                param,
                offset,
                (pager.options.limit + 1)
            ];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get units success',
                    units: createPager(result, page, param)
                });
            }).catch((err) => {
                return reject({code: 404, success: false, message: 'Get units failed', units: err});
            });

        });
    },
    getByStatus: ({ data, command = commands.GET_BY_STATUS }) => {
        return new Promise((resolve, reject) => {

            let query = queryConfig(data, command);

            DB.raw(query.next().value, query.next().value).then((result) => {
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Get units success',
                        units: {
                            data: transformer.transformUnit(result)
                        }
                    });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Get units failed', units: err});
                });

        });
    },
    save: ({ data, transform = true, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {

            let query = queryConfig(data, command);

            DB.raw(query.next().value, query.next().value).then((result) => {
                    if (command === commands.UPDATE) 
                        Object.assign({}, ...data);
                    
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Save unit success',
                        units: {
                            data: transform
                                ? transformer.transformUnit([data])
                                : [data]
                        }
                    });
                })
                .catch((err) => {
                    throw err;
                    return reject({code: 404, success: false, message: 'Save unit failed', units: err});
                });
        });
    },
    remove: ({
        data,
        command = commands.UPDATE
    }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB
                .raw(query.next().value, query.next().value)
                .then((result) => {
                    unit
                        .get({})
                        .then((units) => {
                            return resolve({code: 200, success: true, message: 'Remove units success', units: units.units});
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject({code: 404, success: false, message: 'Remove units failed', units: err});
                });
        });
    }
}
