const _ = require('lodash');

const moment = require('moment');

const appUser = require('../auth/user');

const DB = require('./db');

const RawQueries = require('./raw_queries');

const analysis = module.exports = {

    analyseBuilding: ({ id = undefined }) => {
        return new Promise((resolve, reject) => {

            // appUser.user.pid
            DB.exec('CALL `get_occupancy_per_building`(?);', [appUser.user.pid]).then((result) => {

                const buildings = _.uniqBy(result, 'building_name').map((building) => {
                    return building.building_name;
                });

                let buildingOccupancy = _(result).groupBy('building_name');

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Building analysis success',
                    buildingAnalysis: {
                        countBuildings: _(buildings).size(),
                        buildings: buildings,
                        occupancy: buildingOccupancy
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get building analysis failed', buildingAnalysis: err });
            });

        });
    },
    analyseUnit: ({ id = undefined }) => {
        return new Promise((resolve, reject) => {

            // appUser.user.pid
            DB.exec('CALL `analyse_unit`(?);', [appUser.user.pid]).then((result) => {

                let unitOccupancy = _(result).groupBy('status').map((unit, key) => {
                    return {
                        status: key,
                        status_count: _.sumBy(unit, 'count_status')
                    };
                }).value();

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Unit analysis success',
                    unitAnalysis: {
                        countUnits: _.sumBy(result, 'count_status'),
                        occupancy: unitOccupancy
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get unit analysis failed', unitAnalysis: err });
            });

        });
    },
    analyseShifts: ({ shiftId = undefined }) => {
        return new Promise((resolve, reject) => {

            // appUser.user.pid
            DB.exec('CALL `analyse_shifts`(?,?,?);', [appUser.user.pid, appUser.user.sid, shiftId]).then((result) => {

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Shifts analysis success',
                    shiftsAnalysis: result
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get shifts analysis failed', shiftsAnalysis: err });
            });

        });
    },
    analysePayment: ({ id = undefined }) => {
        return new Promise((resolve, reject) => {

            RawQueries.analysePayment({}).then((result) => {

                const groupedByDiscriminator = _(result).groupBy('discriminator').value();
                const cummulativeBalance = _.first(groupedByDiscriminator.cummulative).unpaid
                const monthlyBalances = _.get(groupedByDiscriminator, 'month').map((monthlyBalance) => {
                    return {
                        month: monthlyBalance.pay_month,
                        balance: monthlyBalance.unpaid
                    }
                });

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Payment analysis success',
                    paymentAnalysis: {
                        cummulativeBalance: cummulativeBalance,
                        monthlyBalances: monthlyBalances
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get payment analysis failed', paymenttAnalysis: err });
            });

        });
    },
    collectionsByPaymentMode: ({ year = new Date().getFullYear }) => {
        return new Promise((resolve, reject) => {

            RawQueries.collectionsByPaymentMode({ year: year }).then((result) => {

                const collectionsByPaymentMode = _(result).orderBy('amount', ['desc']).value();

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Collections by payment mode success',
                    collectionsByPaymentMode: {
                        collectionsByPaymentMode: collectionsByPaymentMode
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Collections by payment mode failed', collectionsByPaymentMode: err });
            });

        });
    },
    collectionsByUnitType: ({ year = new Date().getFullYear }) => {
        return new Promise((resolve, reject) => {

            RawQueries.collectionsByUnitType({ year: year }).then((result) => {

                const collectionsByUnitType = _(result).orderBy('amount', ['desc']).value();

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Collections by payment mode success',
                    collectionsByUnitType: {
                        collectionsByUnitType: collectionsByUnitType
                    }
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Collections by payment mode failed', collectionsByPaymentMode: err });
            });

        });
    },
    collectionsByApartmentPerYear: ({ year = new Date().getFullYear() }) => {
        return new Promise((resolve, reject) => {
            RawQueries.collectionsByApartmentPerYear({ year: year }).then((result) => {

                const groupedByBuilding = _(result).groupBy('buildingName').map((buildingGroup, building) => {
                    return {
                        metadata: {
                            buildingName: building,
                            year: _.first(buildingGroup).year,
                            yearTotal: _.first(buildingGroup).yearly_total
                        },
                        months: buildingGroup.map((month) => {
                            return {
                                jan: month.jan,
                                feb: month.feb,
                                mar: month.mar,
                                apr: month.apr,
                                may: month.may,
                                jun: month.jun,
                                jul: month.jul,
                                aug: month.aug,
                                sep: month.sep,
                                oct: month.oct,
                                nov: month.nov,
                                dec: month.dec
                            }
                        })
                    }
                });

                return resolve({
                    code: 200,
                    success: true,
                    message: 'Collection analysis per apartment',
                    collectiontAnalysisPerApartmentPerYear: {
                        collections: groupedByBuilding
                    }
                });

            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Collection analysis per apartment', collectiontAnalysisPerApartment: err });
            });
        });
    }
}
