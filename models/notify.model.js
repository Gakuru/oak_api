const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const _ = require('lodash');

const appUser = require('../auth/user');

const DB = require('./db');

const RawQueries = require('./raw_queries');

const querystring = require('querystring');
const https = require('https');

const moment = require('moment');

// Your login credentials
let username = process.env.AFRICAS_TALKING_USERNAME;
let apikey = process.env.AFRICAS_TALKING_KEY;

let post_options = {
    host: 'api.africastalking.com',
    path: '/version1/messaging',
    method: 'POST',

    rejectUnauthorized: false,
    requestCert: true,
    agent: false,

    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': undefined,
        'Accept': 'application/json',
        'apikey': apikey
    }
};


/***@todo move to a service of its own  */
const generateQrCode = (text) => {
    const qr = require('qr-image');
    const fs = require('fs');    
    const type = 'png';
    const qrCode = qr.image(text, { type });
    const name = `${text}_${moment().format('YYYY_MM_DD_HH_mm_ss')}.${type}`;
    const path = `/var/www/html/qrcodes/${name}`;
    const output = fs.createWriteStream(path);
    qrCode.pipe(output);
    return name;
}


const NOTIFICATION_CATEGORIES = {
    smsNotifications:'smsNotifications'
}

const SMS = {
    smsCost: 2,    
    standardLength: 140,
    smsPrice: 2
}

NOTIFIACTION_ROUTES = {
    sms:'sms',
    email:'email'
}

const notification = module.exports = {

    sendUtilityReadings: ({ route }) => {
        return new Promise((resolve, reject) => {

            switch (route) {
                case NOTIFIACTION_ROUTES.sms: {
                    notification.notifyBySMS().then((result) => {
                        return resolve({
                            code: 200,
                            success: true,
                            message: 'Sent by SMS utility Readings'
                        });
                    }).catch((err) => {
                        return reject({code: 404, success: false, message: 'failed to SMS Utility Readings', notifications: err});
                    });             
                }
                case NOTIFIACTION_ROUTES.email: {
                    break;
                }
                default: {
                    return reject({code: 404, success: false, message: 'Get building analysis failed', notifications: err});
                }
            }
        });
    },
    notifyBySMS: () => {
        return new Promise((resolve, reject) => {

            let postData = {
                postErrors: []
            };

            DB.exec('CALL `water_sms_info`(?);', [appUser.user.pid]).then((result) => {

                result.forEach(sms => {                    

                    let post_data = querystring.stringify({ 'username': username, 'to': sms.mobileNo, 'message': sms.message });

                    notification.sendSMS(sms.tenant_id, sms.mobileNo, sms.message).then((result) => {
                        connection.query('UPDATE `documents` SET `notified`=? WHERE `id`=?', [1, sms.document_id], (err, result, fields) => {
                            if (err) throw err;
                            connection.query('UPDATE `utility_readings` SET `notified`=? WHERE `id`=?', [1, sms.reading_id], (err, result, fields) => {
                                if (err) throw err;

                                return resolve(true);
                            });
                        });
                    }).catch((err) => {
                        return resolve(err);
                    });
                    
                });
                
            }).catch((err) => {
                return reject(err);
            })

            
        });
    }, checkSMSSetting: async (smsSettingKey, pid = appUser.user.pid) => {
        if (smsSettingKey) {
            return await Promise.resolve(DB.raw('SELECT `settings_value` FROM `settings` WHERE `category`=? AND `settings_key`=? AND `pid`=?', [NOTIFICATION_CATEGORIES.smsNotifications,smsSettingKey,pid]).then((result) => {
                if (!_.isEmpty(result))
                    return Promise.resolve(Boolean(_.first(result).settings_value));
                
                return Promise.resolve(false);
            }));
        }
        return await Promise.resolve(false);
    },
    sendSMS: (tenantId, to, message, pid = appUser.user.pid) => {
        return new Promise((resolve, reject) => {
            notification._SendSMS(tenantId, to, message, pid).then((result) => {
                    if(result)
                        return resolve(result);
                    return resolve({ sent: true });
            });
        });
    },
    _SendSMS: (tenantId, to, message, pid) => DB.raw('SELECT `amount` FROM `parent_balances` WHERE `pid` = ?', [pid]).then((balance) => {

        if (!_.isEmpty(balance)) {
            balance = _.first(balance).amount;
            // Calculate the sms content length
            const smsContentLength = to.length + message.length;
            const smsParts = Math.ceil(smsContentLength / SMS.standardLength);

            SMS.smsPrice = (smsParts > 1 ? (SMS.smsCost * smsParts) : SMS.smsCost);
            
            if (balance > 0 && balance >= SMS.smsPrice) {
                let post_data = querystring.stringify({username,to,message});

                /** Remeber to append the lendth of the message */
                post_options.headers["Content-Length"] = post_data.length;

                let post_req = https.request(post_options, (res) => {
                    res.setEncoding('utf8');
                    res.on('data', (chunk) => {
                        let jsObject = JSON.parse(chunk);
                        let recipients = jsObject.SMSMessageData.Recipients;
                        if (recipients.length) {
                            recipients.forEach((recipient) => {
                                // Log and persist the balance
                                balance = -Math.abs(SMS.smsPrice);
                                notification.logSMS({ balance:balance,tenant_id: tenantId, message: message, cost: recipient.cost, status: recipient.status, pid: pid });                                    
                            });
                        } else {
                            // console.log('Error while sending: ' + jsObject.SMSMessageData.Message);
                            postData
                                .postErrors
                                .push('Error while sending: ' + jsObject.SMSMessageData.Message);
                            
                            return Promise.reject(postData.postErrors);
                        }
                    });
                });

                // Add post parameters to the http request
                post_req.write(post_data);

                post_req.end();
            } else {
                // Account has no enough balance, notify the user of this
                // console.log('Account has no enough balance, notify the user of this');
                return Promise.resolve({sent:false,message:'Account has no enough balance'});
            }
        } else {
            // Account has no balance, notify the user of this
            // console.log('Account has no balance, notify the user of this');
            return Promise.resolve({sent:false,message:'Account has no balance'});
        }
            
    }),
    sendEmail: (recipients, subject, body, attachments=[]) => {
        return new Promise((resolve, reject) => {
            if (recipients.length) {
                const nodemailer = require('nodemailer');
                
                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                    user: process.env.GMAIL_USERNAME,
                    pass: process.env.GMAIL_PASSWORD
                    }
                });
                
                const mailOptions = {
                    from: `RenterKe <renter.co.ke@gmail.com>`,
                    to: recipients,
                    subject: subject,
                    html: body,
                    attachments:attachments
                };
            
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return reject(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                        return resolve(true);
                    }
                });
                
            } else {
                return resolve(true);
            }
        });
    },
    logSMS: ({tenant_id,message,cost,status,balance, pid = appUser.user.pid}) => {
        return new Promise((resolve, reject) => {
            //persist the balance
            DB.exec('CALL `insert_parent_balance`(?,?,?);', [uuid(), pid, balance]).then((result) => {
                connection.query('CALL `insert_sms_log` (?,?,?,?,?,?);', [uuid(), pid, tenant_id, message, cost, status], (err, result, fields) => {
                    if (err) throw err;
    
                    return resolve(true);
                });
            });
        });
    }
}
