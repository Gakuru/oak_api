module.exports = {
    transformFuelType: (fuelTypes) => {
        return fuelTypes.map((fuelType) => {
            return {
                id: fuelType.id,
                fuelTypeName: fuelType.name,
                fuelTypePrice: fuelType.price,
            }
        });
    },
    transformStorages: (storages) => {
        return storages.map((storage) => {
            return {
                id: storage.id,
                storageName: storage.name,
                storageCapacity: storage.total_capacity,
                storageRemainingCapacity: storage.in_capacity,
                fuelType: storage.fuelType ? JSON.parse(storage.fuelType) : [],
            }
        });
    },
    transformDrops: (drops) => {
        return drops.map((drop) => {
            return {
                id: drop.id,
                purchasePrice: drop.purchase_price ? drop.purchase_price : 0,
                fuelType: {
                    fuelTypeId: drop.fuel_type_id,
                    fuelTypeName: drop.fuelTypeName
                },
                storage: {
                    storageTankId: drop.storage_id,
                    storageName: drop.storageTankName
                },
                quantity: drop.quantity,
                dropDate: drop.drop_date,
            }
        });
    },
    transformShifts: (shifts) => {
        return shifts.map((shift) => {
            return {
                id: shift.id,
                shiftName: shift.name,
                shiftFrom: shift.period_from,
                shiftTo: shift.period_to,
            }
        });
    },
    transformPumps: (pumps) => {
        return pumps.map((pump) => {
            return {
                id: pump.id,
                pumpName: pump.name,
                nozzles: pump.nozzles ? JSON.parse(pump.nozzles).map(nozzle => {
                    return {
                        id: nozzle.id,
                        name: nozzle.name,
                        fuelType: {
                            id: nozzle.fuelTypeId,
                            name: nozzle.fuelTypeName,
                            ppu: nozzle.fuelTypePPU
                        },
                        initialReading: nozzle.initialReading ? nozzle.initialReading : 0,
                        previousReading: nozzle.previousReading ? nozzle.previousReading : 0,
                        currentReading: nozzle.currentReading ? nozzle.currentReading : 0,
                    }
                }) : [],
            }
        });
    },
    transformReadings: (readings) => {
        return readings.map((reading) => {
            return {
                id: reading.id,
                pumpId: reading.pump_id,
                pumpName: reading.pump_name,
                shiftId: reading.shift_id,
                shiftName: reading.shift_name,
                noozleId: reading.nozzle_id,
                previousReading: reading.previous_reading,
                currentReading: reading.current_reading,
                unitPrice: reading.unit_price,
                consumption: reading.consumption,
                expectedAmount: reading.expectedAmount,
                nozzles: reading.nozzles ? JSON.parse(reading.nozzles).filter(nozzle => { return nozzle.id === reading.nozzle_id }).map(nozzle => {
                    return {
                        id: nozzle.id,
                        name: nozzle.name,
                        fuelType: {
                            id: nozzle.fuelTypeId,
                            name: nozzle.fuelTypeName
                        }
                    }
                }) : [],
            }
        });
    },
    transformAccountBalance: (result) => {
        return result.map((account) => {
            return {
                id: account.id,
                balance: account.amount
            };
        });
    },
    transformReports: (result) => {
        return result.map((report, i) => {
            return {
                id: (++i),
                reportName: report.reportName,
                reportDescription: report.reportDescription,
                previewUrl: report.previewUrl
            };
        });
    }
}
