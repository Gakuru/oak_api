const express = require('express');
const app = express();
const ports = [2070, 2071];
const path = require('path');
const dotenv = require('dotenv').config({ path: path.join(__dirname, '.env') });
const bodyParser = require('body-parser');


const fuelTypes = require('./routes/fuelTypes.routes');
const storages = require('./routes/storages.routes');
const drops = require('./routes/drops.routes');
const shifts = require('./routes/shifts.routes');
const pumps = require('./routes/pumps.routes');
const readings = require('./routes/readings.routes');

const notify = require('./routes/notify.routes');
const analysis = require('./routes/analysis.routes');
const account = require('./routes/account.routes');
const reports = require('./routes/reports.routes');
const user = require('./routes/user.routes');

const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

const auth = require('./auth/auth');

app.set('trust proxy', 'loopback');

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use((req, res, next) => {
  if (req.headers['x-access-token']) {
    jwt
      .verify(req.headers['x-access-token'], 'secret', function (err, decode) {
        if (err)
          req.token = undefined;
        req.token = decode;
        next();
      });
  } else {
    req.token = undefined;
    next();
  }
});


app.use('/', user);
app.use('/', account);

app.use('/', auth.verify, fuelTypes);
app.use('/', auth.verify, storages);
app.use('/', auth.verify, drops);
app.use('/', auth.verify, shifts);
app.use('/', auth.verify, pumps);
app.use('/', auth.verify, readings);
app.use('/', auth.verify, analysis);

// app.use('/', auth.verify, analysis);
// app.use('/', auth.verify, fuelTypes);
// app.use('/', auth.verify, notify);
// app.use('/', auth.verify, account);
// app.use('/', auth.verify, reports);

ports.forEach((port, i) => {
  app.listen(port, () => {
    console.log('manage api listening on port %s', port);
  });
});