
const appUser = require('../auth/user');

const notificationAction = {
    REMIND_ALL: 'REMIND_ALL'
}

const idKeys = {
    INVOICE_ID: 'INVOICE_ID',
    TENANT_ID: 'TENANT_ID'
}

const notificationRoute = {
    SMS: 'SMS',
    EMAIL: 'EMAIL'
}

const reducedIdKeys = (key) => {
    return key;
}

const reducedNotificationRoutes = (route) => {
    return route;
}

const notifyAdapter = module.exports = {
    reminder: ({ data }) => {
        return new Promise((resolve, reject) => {
            const amqp = require('amqplib/callback_api');
            const arrears = require('../models/arrears.model');
            arrears.getAllInArrears({id:data.id}).then((inArrears) => {
                switch (data.route) {
                    case notificationRoute.EMAIL:
                        {
                            //Queue the data for emailing                                        
                            amqp.connect('amqp://localhost', (err, conn) => {
                                conn.createChannel((err, ch) => {
                                    if (err)
                                        throw err;

                                    const msgs = {
                                        EMAIL_BALANCE_SUMMARIES: 'EMAIL_BALANCE_SUMMARIES'
                                    }

                                    const q = 'defaultQueue';
                                    const msg = JSON.stringify({ msg: msgs.EMAIL_BALANCE_SUMMARIES, appUser: appUser, data: inArrears });
                                    
                                    ch.assertQueue(q, { durable: false });
                                    ch.sendToQueue(q, Buffer.from(msg));
                                    setTimeout(() => { conn.close(); return; }, 500);
                                });
                            });
                            return resolve({code:200,success:true,message:'Reminder sent via email'});
                        }
                    case notificationRoute.SMS:
                        {
                            arrears.getAllInArrears({id:data.id}).then((inArrears) => {
                                //Queue the data for sms
                                amqp.connect('amqp://localhost', (err, conn) => {
                                    conn.createChannel((err, ch) => {
                                        if (err)
                                            throw err;

                                        const msgs = {
                                            SMS_BALANCE_SUMMARIES: 'SMS_BALANCE_SUMMARIES'
                                        }

                                        const q = 'defaultQueue';
                                        const msg = JSON.stringify({ msg: msgs.SMS_BALANCE_SUMMARIES, appUser: appUser, data: inArrears });
                                        
                                        ch.assertQueue(q, { durable: false });
                                        ch.sendToQueue(q, Buffer.from(msg));
                                        setTimeout(() => { conn.close(); return; }, 500);
                                    });
                                });
                            });
                            return resolve({code:200,success:true,message:'Reminder sent via sms'});
                        }
                    default:
                        {
                            return resolve({code:200,success:true,message:'Reminders sent'});
                        }
                }

            });
        });
    }
}