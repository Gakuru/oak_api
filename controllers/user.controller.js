const User = require('../models/user.model');

const COMMAND = require('../config/commands/commands');

module.exports = {
    get: (args) => {
        const {id, callback} = args;
        User.get(id).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    find: (args) => {
        const {param, callback} = args;
        User.find(param).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    create: (args) => {
        const {data, callback} = args;
        User.save(data, COMMAND.SAVE).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    authenticate: (args) => {
        const { data, callback } = args;
        User.authenticate(data).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    tag: (args) => {
        const {data, callback} = args;
        User.save(data, COMMAND.TAG).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    groupAssign: (args) => {
        const {data, callback} = args;
        User.save(data, COMMAND.GROUPASSIGN).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    update: (args) => {
        const {data, callback} = args;
        User.save(data, COMMAND.UPDATE).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    },
    destroy: (args) => {
        const {data, callback} = args;
        User.remove(data, COMMAND.TRASH).then(result => {
            callback(result);
        }).catch(err => {
            callback(err);
        });
    }
};