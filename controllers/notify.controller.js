const notify = require('../models/notify.model');
const notifyAdapter = require('../adapters/notify.adapter');

module.exports = {
  sendUtilityReadings: ({ data, callback }) => {
    notify.sendUtilityReadings(data).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  reminder: ({ data, callback }) => {
    notifyAdapter.reminder({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  }
};
