const fuelType = require('../models/fuelTypes.model');

const command = require('../config/commands/commands');

module.exports = {
  get: ({ page, id, callback }) => {
    fuelType.get({ page, id, command: id ? command.GET_BY_ID : command.GET }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  find: ({ param, page, callback }) => {
    fuelType.find({ param, page }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  create: ({ data, callback }) => {
    fuelType.save({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  update: ({ data, callback }) => {
    fuelType.save({ data, command: command.UPDATE }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  remove: ({ data, callback }) => {
    fuelType.remove({ data }).then((result) => {
      callback(result)
    }).catch((err) => {
      callback(err);
    });
  },
};
