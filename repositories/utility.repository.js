const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const appUser = require('../auth/user');

const moment = require('moment');

const lineType = {
    utility: {
        name: undefined,
        enumValue: 102
    },
    rent: {
        name: undefined,
        enumValue: 101,
    }

}

module.exports = {
    recordUtilityReading: ({ data }) => {

        return new Promise((resolve, reject) => {

            connection.getConnection((err, connection) => {

                connection.beginTransaction((err) => {

                    data.id = uuid();
                    data.pid = appUser.user.pid;
                    data.previous_reading = data.initial_reading ? data.current_reading : data.previous_reading;
                    data.reading_date = moment(data.reading_date,'DD-MM-YYYY').format('YYYY-MM-DD');

                    connection.query('INSERT INTO `utility_readings` SET ?', data, (err, result, fields) => {
                      if (err) 
                        return reject({
                            code: 404,
                            success: false,
                            message: 'Record utility reading failed',
                            tenants: {
                                data: err
                            }
                        });
                        
                        data.id = uuid(); 
                        const {id,pid,tenant_id, utility_id, previous_reading, current_reading, updated_at} = data;
                        connection.query('CALL `insert_tenant_reading`(?,?,?,?,?,?);',
                            [
                                id, pid, tenant_id, utility_id, previous_reading, current_reading
                            ],(err, result, fields) => {
                            if (err)
                                return reject({
                                    code: 404,
                                    success: false,
                                    message: 'Record utility reading failed',
                                    tenants: {
                                        data: err
                                    }
                                });
                        });
                    });

                    // Commit
                    connection.commit((err) => {
                        if (err) {
                            return connection.rollback(() => {
                                return reject({
                                    code: 404,
                                    success: false,
                                    message: 'Record utility reading failed',
                                    tenants: {
                                        data: err
                                    }
                                });
                            });
                        } else {
                            return resolve({
                                code: 200,
                                success: true,
                                iterable: false,
                                message: 'Record utility reading suceess',
                                tenants: {
                                    data:[data]
                                }
                            });
                        }
                    });
                });
                connection.release();
            });

        });
    }
}
