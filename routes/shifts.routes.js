const express = require('express');
const router = express.Router();
const shifts = require('../controllers/shifts.controller');
const appName = require('../utils/common').AppName;
const moment = require('moment');

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/shifts', (req, res) => {
  shifts.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/shifts/:id', (req, res) => {
  shifts.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/shifts/search/:param', (req, res) => {
  shifts.find({
    param: req.params.param,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/shifts/search/:param/page/:page', (req, res) => {
  shifts.find({
    param: req.params.param,
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/shifts/page/:page', (req, res) => {
  shifts.get({
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/shifts', (req, res) => {
  shifts.create({
    data: {
      name: req.body.shiftName,
      period_from:  moment(req.body.shiftFrom).format('HH:mm'),
      period_to:  moment(req.body.shiftTo).format('HH:mm'),
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/shifts', (req, res) => {
  shifts.update({
    data: [{
      name: req.body.shiftName,
      period_from:  moment(req.body.shiftFrom).format('HH:mm'),
      period_to:  moment(req.body.shiftTo).format('HH:mm'),
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/shifts', (req, res) => {
  shifts.remove({
    data: [{
      deleted: true
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});


module.exports = router;