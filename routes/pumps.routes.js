const express = require('express');
const router = express.Router();
const pumps = require('../controllers/pumps.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/pumps', (req, res) => {
  pumps.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/pumps/:id', (req, res) => {
  pumps.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/pumps/search/:param', (req, res) => {
  pumps.find({
    param: req.params.param,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/pumps/search/:param/page/:page', (req, res) => {
  pumps.find({
    param: req.params.param,
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/pumps/page/:page', (req, res) => {
  pumps.get({
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/pumps/nozzle/:id', (req, res) => {
  pumps.getNozzle({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/pumps', (req, res) => {
  pumps.create({
    data: {
      name: req.body.pumpName,
      nozzles: req.body.nozzles,
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/pumps', (req, res) => {
  pumps.update({
    data: {
      name: req.body.pumpName,
      nozzles: req.body.nozzles,
      id: req.body.id
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/pumps', (req, res) => {
  pumps.remove({
    data: [{
      deleted: true
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/pumps/nozzle/:id', (req, res) => {
  pumps.removeNozzle({
    data: [{
      deleted: true
    }, {
      id: req.params.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});


module.exports = router;