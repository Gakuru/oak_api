const express = require('express');
const router = express.Router();
const notify = require('../controllers/notify.controller');

router.get('/', (req, res) => {
    res.send('RenterKE');
});

router.post('/notify', (req, res) => {
    notify.sendUtilityReadings({
        data: {
            route:req.body.route
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/notify/reminder', (req, res) => {
    notify.reminder({
      data: {
          route: req.body.notificationMethod,
          notificationAction: req.body.notificationAction,
          id: req.body.id,
      },
      callback: (result) => {
        res.json(result);
      }
    });
  });

module.exports = router;
