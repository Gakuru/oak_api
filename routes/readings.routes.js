const express = require('express');
const router = express.Router();
const readings = require('../controllers/readings.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/readings', (req, res) => {
  readings.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/readings/:id', (req, res) => {
  readings.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/readings/search/:param', (req, res) => {
  readings.find({
    param: req.params.param,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/readings/search/:param/page/:page', (req, res) => {
  readings.find({
    param: req.params.param,
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/readings/page/:page', (req, res) => {
  readings.get({
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/readings', (req, res) => {
  readings.create({
    data: {
      noozleId: req.body.nozzleId,
      shiftId: req.body.shiftId,
      previousReading: req.body.previousReading,
      currentReading: req.body.currentReading,
      unitPrice: req.body.pricePerLitre,
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/readings', (req, res) => {
  readings.update({
    data: [{
      name: req.body.buildingName,
      location: req.body.buildingLocation,
      returns_enabled: req.body.returnsEnabled,
      ownerName: req.body.ownerName,
      ownerEmail: req.body.ownerEmail,
      ownerPhone: req.body.ownerPhone,
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/readings', (req, res) => {
  readings.remove({
    data: [{
      deleted: true
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});


module.exports = router;