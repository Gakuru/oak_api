const express = require('express');
const router = express.Router();
const fuelType = require('../controllers/fuelTypes.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/fueltypes', (req, res) => {
  fuelType.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/fueltypes/:id', (req, res) => {
  fuelType.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/fueltypes/search/:param', (req, res) => {
  fuelType.find({
    param: req.params.param,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/fueltypes/search/:param/page/:page', (req, res) => {
  fuelType.find({
    param: req.params.param,
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/fueltypes/page/:page', (req, res) => {
  fuelType.get({
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/fueltypes', (req, res) => {
  fuelType.create({
    data: {
      name: req.body.fuelTypeName,
      price: req.body.fuelTypePrice,
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/fueltypes', (req, res) => {
  fuelType.update({
    data: [{
      name: req.body.fuelTypeName,
      price: req.body.fuelTypePrice,
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/fueltypes', (req, res) => {
  fuelType.remove({
    data: [{
      deleted: true
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});


module.exports = router;