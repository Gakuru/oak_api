const express = require('express');
const router = express.Router();
const drops = require('../controllers/drops.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/drops', (req, res) => {
  drops.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/drops/:id', (req, res) => {
  drops.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/drops/search/:param', (req, res) => {
  drops.find({
    param: req.params.param,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/drops/search/:param/page/:page', (req, res) => {
  drops.find({
    param: req.params.param,
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/drops/page/:page', (req, res) => {
  drops.get({
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/drops', (req, res) => {
  drops.create({
    data: {
      drop_date: req.body.dropDate,
      fuel_type_id: req.body.fuelTypeId,
      storage_id: req.body.storageTankId,
      quantity: req.body.quantity,
      purchase_price: req.body.purchaseUnitPrice,
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/drops', (req, res) => {
  drops.update({
    data: [{
      name: req.body.buildingName,
      location: req.body.buildingLocation,
      returns_enabled: req.body.returnsEnabled,
      ownerName: req.body.ownerName,
      ownerEmail: req.body.ownerEmail,
      ownerPhone: req.body.ownerPhone,
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/drops', (req, res) => {
  drops.remove({
    data: [{
      deleted: true
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});


module.exports = router;