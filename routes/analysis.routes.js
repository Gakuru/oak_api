const express = require('express');
const router = express.Router();
const analysis = require('../controllers/analysis.controller');

router.get('/', (req, res) => {
    res.send('RenterKE');
});

router.get('/analysis/shifts/:shiftId?', (req, res) => {
    analysis.analyseShifts({
        shiftId: req.params.shiftId,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/analysis/unit', (req, res) => {
    analysis.analyseUnit({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/analysis/payment', (req, res) => {
    analysis.analysePayment({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/analysis/collectionsbypaymentmode/:year', (req, res) => {
    analysis.collectionsByPaymentMode({
        year: req.params.year,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/analysis/collectionsbyunittype/:year', (req, res) => {
    analysis.collectionsByUnitType({
        year: req.params.year,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/analysis/collectionanalysisperaptmentperyear/:year', (req, res) => {
    analysis.collectionsByApartmentPerYear({
        year: req.params.year,
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;
