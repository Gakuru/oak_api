const express = require('express');
const router = express.Router();
const storage = require('../controllers/storages.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/storages', (req, res) => {
  storage.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/storages/:id', (req, res) => {
  storage.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/storages/search/:param', (req, res) => {
  storage.find({
    param: req.params.param,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/storages/search/:param/page/:page', (req, res) => {
  storage.find({
    param: req.params.param,
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/storages/page/:page', (req, res) => {
  storage.get({
    page: req.params.page,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/storages', (req, res) => {
  storage.create({
    data: {
      name: req.body.storageName,
      fuel_type_id: req.body.storageFuelTypeId,
      total_capacity: req.body.storageCapacity,
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/storages', (req, res) => {
  storage.update({
    data: [{
      name: req.body.storageName,
      fuel_type_id: req.body.fuelType.id,
      total_capacity: req.body.storageCapacity,
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});

router.delete('/storages', (req, res) => {
  storage.remove({
    data: [{
      deleted: true
    }, {
      id: req.body.id
    }],
    callback: (result) => {
      res.json(result);
    }
  });
});


module.exports = router;