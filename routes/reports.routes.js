const express = require('express');
const router = express.Router();
const report = require('../controllers/reports.controller');

router.get('/', (req, res) => {
  res.send('RenterKE');
});

router.get('/reports', (req, res) => {
  report.getReports({
    callback: (result) => {
        res.json(result);
    }
  });
});

router.get('/report/render/:reporturi?', (req, res) => {
  report.getReportUrl({
    data: {
      reporturi:req.params.reporturi
    },
    callback: (result) => {
      res.send(
        `
          <iframe src="${result.reportUrl}"
                style="height:70vh; width:100%;" frameBorder="0">
                <p>Your browser does not support iframes.</p>
          </iframe>
        `
      );
    }
  });
});

module.exports = router;
